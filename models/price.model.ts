export interface Price {
  ht: Number;
  ttc: Number;
}
