import { User } from './user.model';
import { Place } from './place.model';
import { Site } from './site.model';

export interface Quotation {
  uid: string;
  customer: User;
  number: number;
  nbCollaborators: number;
  flags: {
    hasBeenSigned: boolean;
    hasKitchen: boolean;
    needEquipment: boolean;
    needSanitaryEquipment: boolean;
  };
  yousign: {
    procedureId: string;
    fileId: string;
  };
  keyRecovaration: string;
  interventionPlace: Site;
  interventionDays: Array<number>;
  interventionStartTime: string;
  interventionFirstInterventionTime: string;
  interventionDuration: number;
  surface: number;
  totalNbHoursByIntervention: number;
  totalNbHoursByWeek: number;
  totalNbHoursByMonth: number;
  totalNbInterventionsByWeek: number;
  totalNbInterventionsByMonth: number;
  totalEquipmentHT: number;
  priceByCollaborator: number;
  totalPriceHT: number;
  totalPriceTTC: number;
  pdfPath: string;
  createdAt: Date;
  updatedAt: Date;
} 

