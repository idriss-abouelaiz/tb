
export interface Notification {
  title: string;
  body: string;
  link: string;
  topic: string;
  readBy: Array<string>;
  userIds: Array<string>;
  uid: string;
}