export interface Place {
  geoPoint: Array<Number>;
  street: String;
  postalCode: String;
  city: String;
  fullAddress: String;
}