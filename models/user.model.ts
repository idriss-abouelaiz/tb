import { CompanyType } from '../enums/company-type.enum';
import { Place } from './place.model';

export interface User {
  uid: string;
  place: Place;
  companyName: string;
  siret: string;
  rcs: string;
  socialCapital: Number;
  companyType: CompanyType;
  email: string;
  firstName: string;
  fullName: string;
  lastName: string;
  phoneNumber: string;
  role: string;
  token: string;
  quotationsIds: Array<String>,
  flags: {
    passwordHasBeenSet: boolean;
    isProspect: boolean;
  },
  documents: {
    idCard: string;
  }
}
