import { Place } from './place.model';
import { User } from './user.model';

export interface Site {
  place: Place;
  responsable: User;
  accessCode1: string;
  accessCode2: string;
  accessCode3: string;
  complementaryInformations: string;
}