export enum Role {
  ADMIN = 'admin',
  CUSTOMER = 'customer',
  RESPONSABLE = 'responsable',
  PRO = 'pro'
}