export enum CompanyType {
  IMMO = 'Immo',
  EVENT = 'Event',
  BUREAUX = 'Bureaux'
}

export namespace CompanyType {
  export function values() {
    return Object.keys(CompanyType).filter(
      (type) => isNaN(<any>type) && type !== 'values'
    ).map((key) => CompanyType[key]);
  }
}
