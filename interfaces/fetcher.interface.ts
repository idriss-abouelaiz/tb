export interface IFetcher {
  isFetching: boolean;
  fetchingText?: string;
}